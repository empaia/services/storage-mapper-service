import uuid

import requests


class TestClass:
    def setup_method(self):
        # pylint: disable=W0201
        self.client = requests.Session()
        self.url_base = "http://localhost:8000"

    def test_alive(self):
        r = self.client.get(self.url_base + "/alive")
        assert r.status_code == 200
        assert r.json()["status"] == "ok"

    def test_main(self):
        slide_id = str(uuid.uuid4())
        slide_storage_info = {
            "slide_id": slide_id,
            "storage_type": "fs",
            "storage_addresses": [
                {
                    "address": "folder/slide.tiff",
                    "main_address": True,
                    "storage_address_id": str(uuid.uuid4()),
                    "slide_id": slide_id,
                }
            ],
        }
        r = self.client.post(self.url_base + "/v1/slides/", json=slide_storage_info)
        assert r.status_code == 201
        r = self.client.get(self.url_base + f"/v1/slides/{slide_id}")
        assert r.status_code == 200
        r = self.client.post(self.url_base + "/v1/slides/", json=slide_storage_info)
        assert r.status_code == 409
        assert r.json()["detail"]["note"] == "Storage already defined"
        assert r.json()["detail"]["slide_id"] == slide_id
        r = self.client.delete(self.url_base + f"/v1/slides/{slide_id}")
        assert r.status_code == 200
        r = self.client.get(self.url_base + f"/v1/slides/{slide_id}")
        assert r.status_code == 404
        assert r.json()["detail"]["note"] == "No entry found"
        r = self.client.post(self.url_base + "/v1/slides/", json=slide_storage_info)
        assert r.status_code == 201
        r = self.client.get(self.url_base + f"/v1/slides/{slide_id}")
        assert r.status_code == 200

    def test_main_storage_type_missing(self):
        slide_id = str(uuid.uuid4())
        slide_storage_info = {
            "slide_id": slide_id,
            "storage_addresses": [
                {
                    "address": "folder/slide.tiff",
                    "main_address": True,
                    "storage_address_id": str(uuid.uuid4()),
                    "slide_id": slide_id,
                }
            ],
        }
        r = self.client.post(self.url_base + "/v1/slides/", json=slide_storage_info)
        assert r.status_code == 422
        assert len(r.json()["detail"]) == 1
        assert r.json()["detail"][0]["loc"][1] == "storage_type"
        assert r.json()["detail"][0]["type"] == "missing"

    def test_main_try_two_main_files(self):
        slide_id = str(uuid.uuid4())
        slide_storage_info = {
            "slide_id": slide_id,
            "storage_type": "fs",
            "storage_addresses": [
                {
                    "address": "folder/slide.part1.tiff",
                    "main_address": True,
                    "storage_address_id": str(uuid.uuid4()),
                    "slide_id": slide_id,
                },
                {
                    "address": "folder/slide.part2.tiff",
                    "main_address": True,
                    "storage_address_id": str(uuid.uuid4()),
                    "slide_id": slide_id,
                },
            ],
        }
        r = self.client.post(self.url_base + "/v1/slides/", json=slide_storage_info)
        assert r.status_code == 422
        assert len(r.json()["detail"]) == 1
        assert r.json()["detail"][0]["loc"][1] == "storage_addresses"
        assert r.json()["detail"][0]["msg"] == "Value error, Only one storage address can be the main address"

    def test_main_get_unknown_slide_id(self):
        r = self.client.get(self.url_base + "/v1/slides/unknown_id")
        assert r.status_code == 404
        assert r.json()["detail"]["note"] == "No entry found"
        assert r.json()["detail"]["slide_id"] == "unknown_id"

    def test_main_post_same_storage_id(self):
        slide_id = str(uuid.uuid4())
        storage_address_id = str(uuid.uuid4())
        slide_storage_info = {
            "slide_id": slide_id,
            "storage_type": "fs",
            "storage_addresses": [
                {
                    "address": "folder/slide.tiff",
                    "main_address": True,
                    "storage_address_id": storage_address_id,
                    "slide_id": slide_id,
                }
            ],
        }
        slide_id = str(uuid.uuid4())
        other_slide_storage_info = {
            "slide_id": slide_id,
            "storage_type": "fs",
            "storage_addresses": [
                {
                    "address": "folder/slide.tiff",
                    "main_address": True,
                    "storage_address_id": storage_address_id,
                    "slide_id": slide_id,
                }
            ],
        }

        r = self.client.post(self.url_base + "/v1/slides/", json=slide_storage_info)
        assert r.status_code == 201

        r = self.client.post(self.url_base + "/v1/slides/", json=other_slide_storage_info)
        assert r.status_code == 400
        assert r.json()["detail"] == "UniqueViolationError: No unique storage_address_id"
