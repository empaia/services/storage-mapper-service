FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.8@sha256:0c20dd487c2bb040fd78991bf54f396cb1fd9ab314a7a55bee0ad4909748797d AS builder

COPY . /storage_mapper_service
WORKDIR /storage_mapper_service
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.9@sha256:b4e07022de944cd5f676885ce43d36a6c1a58c29bb25a30e024639d0bae158a5

COPY --from=builder /storage_mapper_service/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /storage_mapper_service/dist /artifacts
RUN pip install /artifacts/*.whl

COPY ./run.sh /opt/app/bin/run.sh
