# Changelog

## 0.3.6

- replaced SQLAlchemy client lib with asyncpg

## 0.3.5

- updated dependencies

## 0.3.4

- updated dependencies

## 0.3.3

- updated dependencies

## 0.3.2

- updated dependencies

## 0.3.1

- updated dependencies

## 0.3.0

- pydantic v2 update
- added pydantic-settings
- updated models to 0.4.3

## 0.2.22

- updated dependencies

## 0.2.21

- updated dependencies

## 0.2.20

- updated dependencies

## 0.2.19

- updated dependencies

## 0.2.18

- updated dependencies

## 0.2.17

- updated dependencies

## 0.2.16

- updated dependencies

## 0.2.15

- updated dependencies

## 0.2.14

- updated dependencies

## 0.2.13

- updated dependencies

## 0.2.12

- updated dependencies

## 0.2.11

- updated dependencies

## 0.2.10

- updated dependencies

## 0.2.9

- updated dependencies

## 0.2.8

- updated dependencies
- pinned SQLAlchemy version <2

## 0.2.7

- updated dependencies

## 0.2.6

- updated dependencies

## 0.2.5

- updated dependencies

## 0.2.4

- updated dependencies

## 0.2.3

- updated dependencies

## 0.2.2

- updated dependencies

## 0.2.1

- updated dependencies

## 0.2.0

- introduced new structure to support different api versions
- added api v3
- added cors
- added docker compose based tests
- added alembic database migration
- added tests for database migration

## 0.1.40

- updated dependencies
- pinned postgres 14

## 0.1.39

- updated dependencies

## 0.1.38

- updated dependencies

## 0.1.37

- updated dependencies

## 0.1.36

- updated dependencies

## 0.1.35

- updated dependencies

## 0.1.34

- updated dependencies

## 0.1.33

- updated dependencies

## 0.1.32

- updated dependencies

## 0.1.31

- updated dependencies

## 0.1.30

- updated dependencies

## 0.1.29

- updated dependencies

## 0.1.28

- updated dependencies

## 0.1.27

- updated dependencies

## 0.1.26

- updated dependencies

## 0.1.25

- updated dependencies

## 0.1.24

- updated dependencies

## 0.1.23

- updated dependencies

## 0.1.22

- updated dependencies

## 0.1.21

- updated dependencies

## 0.1.20

- updated dependencies

## 0.1.19

- added error handling for duplicate storage_address_id
- added test
- added logger

## 0.1.18

- updated dependencies

## 0.1.17

- updated dependencies

## 0.1.16

- updated dependencies

## 0.1.15

- updated dependencies

## 0.1.14

- updated dockerfile

## 0.1.13

- added docker image digest

## 0.1.12

- updated ci
- updated dockerfile

## 0.1.11

- updated minor versions

## 0.1.10

- updated postgres from 12 to 14
- updated pytest and pytest-cov

## 0.1.9

- removed timestamps from logs

## 0.1.8

- Added route to delete mappings

## 0.1.7

- Added logging configuration

## 0.1.6

- Added service status model

## 0.1.5

- Extended settings and documentation

## 0.1.4

- Integrated `models` submodule

## 0.1.3

- Fixed missing dev dependencies

## 0.1.2

- Fixed Dockerfile WORKDIR

## 0.1.1

- Added database check on startup
