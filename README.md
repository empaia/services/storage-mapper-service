# Storage Mapper Service

The *Storage Mapper Service* is a simple service that helps users to create and read slide storage information.

## API Versions

### V1 and V3

There are two endpoints made available by this service:

* `POST /vX/slides/` - Create storage information
* `GET /vX/slides/{slide_id}/` - Read storage information

The storage information has the following structure

```json
{
  "slide_id": "4b0ec5e0ec5e5e05ae9e500857314f20",
  "storage_type": "fs",
  "storage_addresses": [
    {
      "storage_address_id": "ed917cbb17ab54ee84152ba30adfb4d5",
      "slide_id": "4b0ec5e0ec5e5e05ae9e500857314f20",
      "address": "Generic TIFF/CMU-1.tiff",
      "main_address": true
    }
  ]
}
```

## How to run

The Storage Mapper Service is a python module and can be run either locally or via docker.

### Run locally

Install Storage Mapper Service by running the following lines within this folder

```bash
poetry install
poetry run production
```

Afterwards, visit [http://localhost:8000/docs](http://localhost:8000/docs) to see an interactive Swagger UI

### Run via docker

Run the docker image

```bash
docker run -it --rm -p 8000:8000 registry.gitlab.com/empaia/services/storage-mapper-service
```

Afterwards, visit [http://localhost:8000/docs](http://localhost:8000/docs) to see an interactive Swagger UI

## Development

### Use debug to activate reload

Service is reloaded after code changes. Activate locally with

```bash
poetry run development
```

### Run tests

```bash
poetry run pytest tests
```

### Codecheck

```bash
poetry run black storage_mapper_service
poetry run isort storage_mapper_service
poetry run pycodestyle storage_mapper_service
poetry run pylint storage_mapper_service
```

### DB Migration

This is covered [here](./storage_mapper_service/db_migration/README.md).
