import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from storage_mapper_service import __version__
from storage_mapper_service.api.root import add_routes_root
from storage_mapper_service.api.v1 import add_routes_v1
from storage_mapper_service.api.v3 import add_routes_v3
from storage_mapper_service.late_init import LateInit
from storage_mapper_service.singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

app = FastAPI(
    title=settings.title,
    description=settings.description,
    version=__version__,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
)
app_v1 = FastAPI(openapi_url=openapi_url)
app_v3 = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app, app_v1, app_v3]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )

late_init = LateInit()

add_routes_root(app, late_init)
add_routes_v1(app_v1, late_init)
add_routes_v3(app_v3, late_init)

app.mount("/v1", app_v1)
app.mount("/v3", app_v3)


@app.on_event("startup")
async def startup_event():
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
