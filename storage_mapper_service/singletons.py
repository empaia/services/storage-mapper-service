import logging

from storage_mapper_service.settings import Settings

settings = Settings()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
