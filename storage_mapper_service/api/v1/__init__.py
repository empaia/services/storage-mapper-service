from storage_mapper_service.api.v1.slides import add_routes_slides


def add_routes_v1(app, late_init):
    add_routes_slides(app, late_init)
