from fastapi import HTTPException, status

from storage_mapper_service.db import db_clients
from storage_mapper_service.models.v3.storage import SlideStorage


def add_routes_slides(app, late_init):
    @app.post(
        "/slides/",
        status_code=status.HTTP_201_CREATED,
        response_model=SlideStorage,
        tags=["Main Routes"],
        summary="Create Slide Storage",
    )
    async def _(slide_storage: SlideStorage):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.conn.transaction():
                return await client.create_slide_storage(slide_storage)

    @app.get(
        "/slides/{slide_id}",
        status_code=status.HTTP_200_OK,
        response_model=SlideStorage,
        tags=["Main Routes"],
        summary="Read Slide Storage",
    )
    async def _(slide_id: str):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.conn.transaction():
                slide_storage = await client.get_slide_storage(slide_id)
                if not slide_storage:
                    raise HTTPException(
                        status_code=404,
                        detail={
                            "note": "No entry found",
                            "slide_id": slide_id,
                        },
                    )
                return slide_storage

    @app.delete(
        "/slides/{slide_id}",
        status_code=status.HTTP_200_OK,
        tags=["Main Routes"],
        summary="Remove Slide Storage",
    )
    async def _(slide_id: str):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.conn.transaction():
                await client.remove_slide_storage(slide_id)
