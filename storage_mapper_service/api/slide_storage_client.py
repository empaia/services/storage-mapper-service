# from typing import Optional

from asyncpg import Connection, PostgresError, UniqueViolationError
from fastapi import HTTPException

from storage_mapper_service import __version__ as version
from storage_mapper_service.models.commons import ServiceStatus, ServiceStatusEnum
from storage_mapper_service.models.v1.storage import SlideStorage, StorageAddress
from storage_mapper_service.singletons import logger


class SlideStorageClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    async def check_server_health(self):
        try:
            await self.conn.execute("SELECT datname FROM pg_database;")
            return ServiceStatus(status=ServiceStatusEnum.OK.value, version=version)
        except PostgresError:
            return ServiceStatus(
                status=ServiceStatusEnum.FAILURE.value, version=version, message="Cannot reach database"
            )

    async def create_slide_storage(self, slide_storage: SlideStorage) -> SlideStorage:
        slide = await self.get_slide_storage(slide_storage.slide_id)
        if slide:
            raise HTTPException(
                status_code=409,
                detail={
                    "note": "Storage already defined",
                    "slide_id": slide_storage.slide_id,
                },
            )

        insert_slide_storage_sql = (
            "INSERT INTO slides_storage ( slide_id, storage_type ) VALUES( $1, $2 ) RETURNING * ;"
        )
        insert_storage_address_sql = """
            INSERT INTO storage_addresses ( storage_address_id, slide_id, address, main_address )
            (SELECT
                st.storage_address_id, st.slide_id, st.address, st.main_address
            FROM
                unnest( $1::storage_addresses[] ) as st
            )
            RETURNING * ;
        """

        try:
            raw_slide_storage = await self.conn.fetchrow(
                insert_slide_storage_sql, slide_storage.slide_id, slide_storage.storage_type
            )
            storage_addresses_to_persist = [
                (addr.storage_address_id, addr.slide_id, addr.address, addr.main_address)
                for addr in slide_storage.storage_addresses
            ]
            result_storage_addresses = []
            raw_storage_addresses = await self.conn.fetch(insert_storage_address_sql, storage_addresses_to_persist)
            for raw_storage_address in raw_storage_addresses:
                result_storage_addresses.append(StorageAddress(**raw_storage_address))
            return SlideStorage(
                slide_id=raw_slide_storage["slide_id"],
                storage_type=raw_slide_storage["storage_type"],
                storage_addresses=result_storage_addresses,
            )
        except UniqueViolationError as e1:
            logger.error("Storage address already defined: %s", e1)
            raise HTTPException(
                status_code=400,
                detail="UniqueViolationError: No unique storage_address_id",
            ) from e1
        except PostgresError as e2:
            logger.error("Failed to persist slide storage: %s", e2)
            raise HTTPException(status_code=500, detail="Failed to persist slide storage") from e2

    async def remove_slide_storage(self, slide_id: str):
        slide = await self.get_slide_storage(slide_id)
        if not slide:
            raise HTTPException(
                status_code=404,
                detail={
                    "note": "No entry found",
                    "slide_id": slide_id,
                },
            )

        delete_storage_addresses_sql = """
            DELETE FROM storage_addresses
            WHERE slide_id = $1;
        """
        delete_slide_storage_sql = """
            DELETE FROM slides_storage
            WHERE slide_id = $1
            RETURNING slide_id ;
        """

        try:
            _ = await self.conn.fetchrow(delete_storage_addresses_sql, slide_id)
            _ = await self.conn.fetch(delete_slide_storage_sql, slide_id)
        except PostgresError as e2:
            logger.error("Failed to delete slide storage: %s", e2)
            raise HTTPException(status_code=500, detail="Failed to delete slide storage") from e2

    async def get_slide_storage(self, slide_id: str) -> SlideStorage:
        sql = """
            SELECT slides_storage.*, storage_addresses.*
            FROM slides_storage
            LEFT JOIN storage_addresses ON slides_storage.slide_id = storage_addresses.slide_id
            WHERE slides_storage.slide_id = $1;
        """

        try:
            raw_storage_addresses = await self.conn.fetch(sql, slide_id)
            if not raw_storage_addresses:
                return None

            storage_addresses = [
                StorageAddress(
                    storage_address_id=addr["storage_address_id"],
                    slide_id=addr["slide_id"],
                    main_address=bool(addr["main_address"]),
                    address=addr["address"],
                )
                for addr in raw_storage_addresses
            ]
            return SlideStorage(
                slide_id=raw_storage_addresses[0]["slide_id"],
                storage_type=raw_storage_addresses[0]["storage_type"],
                storage_addresses=storage_addresses,
            )
        except PostgresError as e1:
            logger.error("Failed to query slide storage: %s", e1)
            raise HTTPException(status_code=500, detail="Failed to query slide storage") from e1
