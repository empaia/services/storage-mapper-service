from typing import Set

from pydantic_settings import BaseSettings

from storage_mapper_service import __version__


class Settings(BaseSettings):
    title: str = "Storage Mapper Service"
    description: str = (
        "The Storage Mapper Service is a simple service that helps users to create and read slide storage information"
    )
    version: str = __version__

    disable_openapi: bool = False
    root_path: str = ""
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = set()
    port: int = 8000
    db_host: str = "sqlite:///./database.db"
    db_port: int = 5432
    db: str = "storage_mappings"
    db_username: str = "empaia_test"
    db_password: str = "A6tP3osxByeM"

    class Config:
        env_file = ".env"
        env_prefix = "sm_"
