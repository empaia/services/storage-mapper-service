from asyncpg import Pool


class AlreadyInitializedError(Exception):
    pass


class LateInit:
    def __init__(self):
        self._pool = None

    @property
    def pool(self):
        return self._pool

    @pool.setter
    def pool(self, pool: Pool):
        if self._pool is not None:
            raise AlreadyInitializedError("Pool already initialized.")

        self._pool = pool
