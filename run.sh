#!/usr/bin/env bash

cdsctl migrate-db && uvicorn storage_mapper_service.app:app $@
